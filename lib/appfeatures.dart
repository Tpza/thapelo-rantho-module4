import 'package:flutter/material.dart';

class AppFeatures extends StatelessWidget {
  const AppFeatures({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('What we learn'),
        centerTitle: true,
      ),
      body: const Center(
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: Text(
              'We have just learned about coding a splash screen and how to get to add dependencies to use animations in our applications.',
              style: TextStyle(fontSize: 20, color: Colors.black)),
        ),
      ),
    );
  }
}
