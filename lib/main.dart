import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:splash/splashscreen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            colorScheme:
                ColorScheme.fromSwatch(primarySwatch: Colors.deepPurple)
                    .copyWith(secondary: Colors.purple)),
        home: AnimatedSplashScreen(
          duration: 1500,
          //  splash: Icons.home,
          nextScreen: const SplashScreen(),
          splashTransition: SplashTransition.slideTransition,
          pageTransitionType: PageTransitionType.leftToRight,
          splash: null,
        ));
  }
}
